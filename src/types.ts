export type Data = {
  todoLists: TodoList[];
};

export type TodoList = {
  id: string;
  name: string;
  todos: Todo[];
};

export type Todo = {
  id: string;
  item: string;
  isCompleted: boolean;
};
