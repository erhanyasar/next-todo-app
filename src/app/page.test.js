import { render } from "@testing-library/react";
import Home from "./page";

test("renders combobox component", () => {
  const { baseElement } = render(<Home />);
  expect(baseElement).toBeTruthy();
});
