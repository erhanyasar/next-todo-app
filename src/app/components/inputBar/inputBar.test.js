import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { InputBar } from "./inputBar";

describe("Check for Rendering InputBar Component", () => {
  test("renders InputBar component", () => {
    const { baseElement } = render(<InputBar />);
    expect(baseElement).toBeTruthy();
  });
});

describe("Check for Component Elements", () => {
  beforeEach(() => render(<InputBar />));

  it("Should find heading", () => {
    const textElement = screen.getByText("To-Do Application");
    expect(textElement).toBeInTheDocument();
  });
  it("Should find text field placeholder", () => {
    const textElement = screen.getAllByText("Type To-Do or a List Name");
    expect(textElement[0]).toBeInTheDocument();
  });
  it("Should find 'Add Todo Item' button label", () => {
    const textElement = screen.getByText("Add Todo Item");
    expect(textElement).toBeInTheDocument();
  });
  it("Should find select field placeholder", () => {
    const textElement = screen.getAllByText("Select a List to Add To-Do");
    expect(textElement[0]).toBeInTheDocument();
  });
  it("Should find select field placeholder", () => {
    const textElement = screen.getByText("* Please select list before adding");
    expect(textElement).toBeInTheDocument();
  });
  it("Should find 'Add Todo List' button label", () => {
    const textElement = screen.getByText("Add Todo List");
    expect(textElement).toBeInTheDocument();
  });
});
