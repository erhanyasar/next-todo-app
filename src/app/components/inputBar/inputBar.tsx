import { ChangeEvent, Dispatch, SetStateAction } from "react";
import {
  Grid,
  Button,
  Stack,
  TextField,
  MenuItem,
  Typography,
} from "@mui/material";
import AddBoxIcon from "@mui/icons-material/AddBox";
import AddToPhotosIcon from "@mui/icons-material/AddToPhotos";
import { v4 as uuidv4 } from "uuid";
import { TodoList } from "@/types";

export const InputBar = ({
  userInput,
  todoLists,
  selectedTodoList,
  setUserInput,
  setTodoLists,
  setSelectedTodoList,
}: {
  userInput: string;
  todoLists: TodoList[];
  selectedTodoList: string;
  setUserInput: Dispatch<SetStateAction<string>>;
  setTodoLists: Dispatch<SetStateAction<TodoList[]>>;
  setSelectedTodoList: Dispatch<SetStateAction<string>>;
}) => {
  const handleInput = (
    e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => setUserInput(e.target.value);

  const handleSelectTodoList = (e: ChangeEvent<HTMLInputElement>) =>
    setSelectedTodoList(e.target.value);

  const handleAddToDoItem = () => {
    setTodoLists((todoLists) => [
      ...todoLists.map((todoList) => {
        if (selectedTodoList === todoList.name)
          return {
            ...todoList,
            todos: [
              ...todoList.todos,
              {
                id: uuidv4(),
                item: userInput,
                isCompleted: false,
              },
            ],
          };
        else return todoList;
      }),
    ]);
    setUserInput("");
  };

  const handleAddToDoList = () => {
    setTodoLists((todoLists) => [
      ...todoLists,
      {
        id: uuidv4(),
        name: userInput,
        todos: [],
      },
    ]);
    setUserInput("");
    // Selects new created list on default
    setSelectedTodoList(userInput);
  };

  return (
    <Grid container spacing={5} maxWidth="100vw">
      <Grid item xs={12}>
        <Stack
          direction="column"
          display="flex"
          alignItems="center"
          justifyContent="center"
        >
          <Typography variant="h4" component="h2" color="primary">
            To-Do Application
          </Typography>
        </Stack>
      </Grid>
      <Grid item xs={12} sm={6} lg={3}>
        <Stack direction="column" spacing={3}>
          <TextField
            variant="outlined"
            value={userInput}
            onChange={handleInput}
            label="Type To-Do or a List Name"
          />
        </Stack>
      </Grid>
      <Grid item xs={12} sm={6} lg={3}>
        <Stack direction="column" spacing={3}>
          <TextField
            select
            label="Select a List to Add To-Do"
            value={selectedTodoList || ""}
            helperText="* Please select list before adding"
            onChange={handleSelectTodoList}
          >
            {todoLists?.map((todoList) => (
              <MenuItem key={todoList.name} value={todoList.name}>
                {todoList.name}
              </MenuItem>
            ))}
            {!todoLists && <MenuItem value=""></MenuItem>}
          </TextField>
        </Stack>
      </Grid>
      <Grid item xs={12} sm={6} lg={3}>
        <Stack direction="column">
          <Button
            variant="contained"
            color="success"
            onClick={handleAddToDoItem}
            startIcon={<AddBoxIcon />}
            disabled={!(userInput && selectedTodoList !== "")}
          >
            Add Todo Item
          </Button>
        </Stack>
      </Grid>
      <Grid item xs={12} sm={6} lg={3}>
        <Stack direction="column">
          <Button
            variant="contained"
            onClick={handleAddToDoList}
            startIcon={<AddToPhotosIcon />}
            disabled={!userInput}
          >
            Add Todo List
          </Button>
        </Stack>
      </Grid>
    </Grid>
  );
};
