import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { ButtonGroup } from "./buttonGroup";

describe("Check for Rendering ButtonGroup Component", () => {
  test("renders ButtonGroup component", () => {
    const { baseElement } = render(<ButtonGroup />);
    expect(baseElement).toBeTruthy();
  });
});
/*
describe("Check for Component Elements", () => {
  beforeEach(() => render(<ButtonGroup />));

  it("Should find 'Persist All' button label", () => {
    const textElement = screen.getByText("Persist All");
    expect(textElement).toBeInTheDocument();
  });
  it("Should find 'Reset All' button label", () => {
    const textElement = screen.getByText("Reset All");
    expect(textElement).toBeInTheDocument();
  });
});
  */
