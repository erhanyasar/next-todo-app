import { Dispatch, SetStateAction } from "react";
import { Button, Stack } from "@mui/material";
import SaveIcon from "@mui/icons-material/Save";
import RestartAltIcon from "@mui/icons-material/RestartAlt";
import { useSnackbar } from "notistack";
import { Data, TodoList } from "@/types";
import _ from "lodash";

export const ButtonGroup = ({
  data,
  todoLists,
  setTodoLists,
  setUserInput,
  setSelectedTodoList,
}: {
  data: Data;
  todoLists: TodoList[];
  setTodoLists: Dispatch<SetStateAction<TodoList[]>>;
  setUserInput: Dispatch<SetStateAction<string>>;
  setSelectedTodoList: Dispatch<SetStateAction<string>>;
}) => {
  const { enqueueSnackbar } = useSnackbar();

  const handleResetTodos = () => {
    setTodoLists(data ? [...data.todoLists] : []);
    setUserInput("");
    setSelectedTodoList("");
  };

  const handlePersistTodos = () =>
    // TODO: Implement persist functionality as a mutation
    enqueueSnackbar("Persist functionality not implemented yet!", {
      variant: "warning",
    });

  return (
    <Stack
      mt={5}
      maxWidth="100vw"
      display="flex"
      flexDirection="row"
      justifyContent="space-between"
      className="button-group-wrapper"
    >
      {!_.isEqual(todoLists, data?.todoLists) && (
        <>
          <Button
            color="secondary"
            variant="contained"
            onClick={handleResetTodos}
            startIcon={<RestartAltIcon />}
            disabled={_.isEqual(todoLists, data?.todoLists)}
          >
            Reset All
          </Button>
          <Button
            color="warning"
            variant="contained"
            onClick={handlePersistTodos}
            startIcon={<SaveIcon />}
          >
            Persist All
          </Button>
        </>
      )}
    </Stack>
  );
};
