import { useState, useEffect } from "react";
import { Box, Container, Grid, CircularProgress } from "@mui/material";
import useSWR from "swr";
import { fetcher } from "../../../utils/fetcher";
import { Data, TodoList } from "@/types";
import Error from "next/error";
import { useSnackbar } from "notistack";
import { InputBar } from "../inputBar/inputBar";
import TodosList from "../todosList/todosList";
import { ButtonGroup } from "../buttonGroup/buttonGroup";

export default function TodosWrapper() {
  const [userInput, setUserInput] = useState("");
  const [todoLists, setTodoLists] = useState<TodoList[]>([]);
  const [selectedTodoList, setSelectedTodoList] = useState("");

  const { enqueueSnackbar } = useSnackbar();
  const { data, error, isLoading } = useSWR<Data>(
    "{ todoLists { id, name, todos { id, item, isCompleted } } }",
    fetcher
  );

  useEffect(() => setTodoLists(data ? [...data.todoLists] : []), [data]);

  const handleDeleteTodoItem = (i: number, j: number) => {
    let selectedTodo = todoLists.find((todoList, index) => {
      if (i === index) return todoList.todos.find((todo, idx) => j === idx);
    });

    let answer = window.confirm(
      `This process is irreversible. Are you sure deleting todo item?\n\n"${selectedTodo?.todos[j].item}"\n\n`
    );

    if (answer) {
      try {
        setTodoLists((todoLists) => [
          ...todoLists.map((todoList, index) => {
            if (index === i)
              return {
                ...todoList,
                todos: [...todoList.todos.filter((todo, idx) => idx !== j)],
              };
            else return todoList;
          }),
        ]);
      } catch (err) {
        enqueueSnackbar("Error while deleting todo!", { variant: "error" });
      } finally {
        enqueueSnackbar("Todo deleted successfully!", { variant: "success" });
      }
    }
  };

  const handlePutBackTodoItem = (i: number, j: number) =>
    setTodoLists((todoLists) => [
      ...todoLists.map((todoList, index) => {
        if (index === i)
          return {
            ...todoList,
            todos: [
              ...todoList.todos.map((todo, idx) => {
                if (idx === j)
                  return {
                    ...todo,
                    isCompleted: false,
                  };
                else return todo;
              }),
            ],
          };
        else return todoList;
      }),
    ]);

  const handleCompleteTodoItem = (i: number, j: number) =>
    setTodoLists((todoLists) => [
      ...todoLists.map((todoList, index) => {
        if (index === i)
          return {
            ...todoList,
            todos: [
              ...todoList.todos.map((todo, idx) => {
                if (idx === j)
                  return {
                    ...todo,
                    isCompleted: true,
                  };
                else return todo;
              }),
            ],
          };
        else return todoList;
      }),
    ]);

  const handleDeleteTodoList = (id: number) => {
    setTodoLists((todoLists) => [
      ...todoLists.filter((todoList, index) => id !== index),
    ]);
  };

  if (error) return <Error statusCode={error.status ?? error.message} />;
  if (isLoading) return <CircularProgress sx={{ margin: "auto" }} />;
  if (!data) return <></>;

  return (
    <Container className="todos-outer-wrapper" maxWidth="xl">
      <style jsx global>
        {`
          .todos-outer-wrapper {
            display: flex;
          }
          .todos-inner-wrapper {
            flex-grow: 1;
            padding-right: 3rem;
          }
        `}
      </style>
      <Box className="todos-inner-wrapper">
        <InputBar
          userInput={userInput}
          todoLists={todoLists}
          selectedTodoList={selectedTodoList}
          setUserInput={setUserInput}
          setTodoLists={setTodoLists}
          setSelectedTodoList={setSelectedTodoList}
        />
        <TodosList
          todoLists={todoLists}
          onDeleteTodo={handleDeleteTodoItem}
          onPutBackTodo={handlePutBackTodoItem}
          onCompleteTodo={handleCompleteTodoItem}
          onDeleteList={handleDeleteTodoList}
        />
        <ButtonGroup
          data={data}
          todoLists={todoLists}
          setTodoLists={setTodoLists}
          setUserInput={setUserInput}
          setSelectedTodoList={setSelectedTodoList}
        />
      </Box>
    </Container>
  );
}
