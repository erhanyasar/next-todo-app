import { render } from "@testing-library/react";
import "@testing-library/jest-dom";
import TodosWrapper from "./todosWrapper";

test("renders TodosWrapper component", () => {
  const { baseElement } = render(<TodosWrapper />);
  expect(baseElement).toBeTruthy();
});
