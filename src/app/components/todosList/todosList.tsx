import {
  Grid,
  Card,
  CardActions,
  CardContent,
  Button,
  Typography,
  Stack,
  Paper,
  Divider,
  IconButton,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import { TodoList } from "@/types";

export default function todosList({
  todoLists,
  onDeleteTodo,
  onPutBackTodo,
  onCompleteTodo,
  onDeleteList,
}: {
  todoLists: TodoList[];
  onDeleteTodo: (i: number, j: number) => void;
  onPutBackTodo: (i: number, j: number) => void;
  onCompleteTodo: (i: number, j: number) => void;
  onDeleteList: (id: number) => void;
}) {
  return (
    <Grid container mt={5}>
      <style jsx global>
        {`
          .todo-card-item {
            min-width: 275px;
            margin: 10px;
          }
          .todo-list {
            padding: 20px;
            background-color: rgb(214, 219, 220);
          }
          @media (max-width: 700px) {
            .todo-list-outer-wrapper {
              flex-direction: column;
            }
            .todo-list-inner-wrapper {
              margin-left: 0 !important;
              margin-top: 24px !important;
            }
          }
        `}
      </style>
      <Stack spacing={3} direction="row" className="todo-list-outer-wrapper">
        {todoLists?.map((todoList, i) => (
          <Grid item key={i} xs={12} className="todo-list-inner-wrapper">
            <Paper elevation={3} className="todo-list">
              <Stack direction="column">
                <Stack
                  display="flex"
                  flexDirection="row"
                  alignItems="center"
                  justifyContent="space-between"
                >
                  <Typography variant="h6" component="h2" color="primary">
                    {todoList.name}
                  </Typography>
                  {!todoList.todos.length && (
                    <IconButton
                      aria-label="delete-list"
                      onClick={() => onDeleteList(i)}
                    >
                      <CloseIcon />
                    </IconButton>
                  )}
                </Stack>
                <Divider />
                {!todoList.todos.length ? (
                  <div className="todo-card-item"></div>
                ) : (
                  <>
                    {todoList.todos.map((todoItem, j) => (
                      <Card key={j} className="todo-card-item">
                        <CardContent>
                          <Typography
                            variant="body1"
                            component="div"
                            sx={{
                              textDecoration: todoItem.isCompleted
                                ? "line-through"
                                : "none",
                            }}
                          >
                            {todoItem.item}
                          </Typography>
                        </CardContent>
                        <CardActions>
                          <Button
                            size="small"
                            color="error"
                            onClick={() => onDeleteTodo(i, j)}
                          >
                            Delete
                          </Button>
                          <Button
                            size="small"
                            color="warning"
                            onClick={() => onPutBackTodo(i, j)}
                            disabled={!todoItem.isCompleted}
                          >
                            Put Back
                          </Button>
                          <Button
                            size="small"
                            color="success"
                            onClick={() => onCompleteTodo(i, j)}
                            disabled={todoItem.isCompleted}
                          >
                            Complete
                          </Button>
                        </CardActions>
                      </Card>
                    ))}
                  </>
                )}
              </Stack>
            </Paper>
          </Grid>
        ))}
      </Stack>
    </Grid>
  );
}
