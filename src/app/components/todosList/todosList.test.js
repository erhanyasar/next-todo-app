import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import TodosList from "./todosList";

describe("Check for Rendering TodosList Component", () => {
  test("renders TodosList component", () => {
    const { baseElement } = render(<TodosList />);
    expect(baseElement).toBeTruthy();
  });
});
/*
describe("Check for Component Elements", () => {
  beforeEach(() => render(<TodosList />));

  it("Should find 'Delete' button label", () => {
    const textElement = screen.getByText("Delete");
    expect(textElement).toBeInTheDocument();
  });
  it("Should find 'Put Back' button label", () => {
    const textElement = screen.getByText("Put Back");
    expect(textElement).toBeInTheDocument();
  });
  it("Should find 'Complete' button label", () => {
    const textElement = screen.getByText("Complete");
    expect(textElement).toBeInTheDocument();
  });
});
*/
