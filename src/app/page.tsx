"use client";

import TodosWrapper from "./components/todosWrapper/todosWrapper";
import styles from "./css/page.module.css";

export default function Home() {
  return (
    <main className={styles.main}>
      <TodosWrapper />
    </main>
  );
}
