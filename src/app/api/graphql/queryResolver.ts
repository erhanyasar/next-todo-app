import { v4 as uuidv4 } from "uuid";

export const todosQueryResolver = [
  {
    id: uuidv4(),
    name: "This is the `first todo list`",
    todos: [
      {
        id: uuidv4(),
        item: "This is the `first todo` field of the root `Query` type",
        isCompleted: true,
      },
      {
        id: uuidv4(),
        item: "This one is second",
        isCompleted: false,
      },
    ],
  },
  {
    id: uuidv4(),
    name: "This is second todo list",
    todos: [
      {
        id: uuidv4(),
        item: "This is my type",
        isCompleted: false,
      },
    ],
  },
];
