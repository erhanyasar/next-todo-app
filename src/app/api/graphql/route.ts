import { createSchema, createYoga } from "graphql-yoga";
import { todosQueryResolver } from "./queryResolver";
import { typeDefs } from "./typeDefs";

const { handleRequest } = createYoga({
  schema: createSchema({
    typeDefs: typeDefs,
    resolvers: {
      Query: {
        todoLists: () => todosQueryResolver,
      },
    },
  }),
  graphqlEndpoint: "/api/graphql",
  fetchAPI: { Response },
});

export { handleRequest as GET, handleRequest as POST };
