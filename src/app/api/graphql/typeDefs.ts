export const typeDefs = `
    type Query {
        todoLists: [TodoList!]
    }

    type TodoList {
        id: String!
        name: String!
        todos: [Todo!]
    }

    type Todo {
        id: String!
        item: String!
        isCompleted: Boolean!
    }
`;
