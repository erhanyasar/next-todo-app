"use client";

import { Suspense } from "react";
import { ThemeProvider, CssBaseline, CircularProgress } from "@mui/material";
import { SnackbarProvider } from "notistack";
import { theme } from "./customTheme";

export default function ThemeRegistry(props: any) {
  const { options, children } = props;

  return (
    <Suspense fallback={<CircularProgress />}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <SnackbarProvider maxSnack={3}>{children}</SnackbarProvider>
      </ThemeProvider>
    </Suspense>
  );
}
