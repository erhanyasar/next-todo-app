# To Do Application

To display this project up & running on production (without needing to do any of the steps below), please visit [Vercel](https://next-todo-app-erhanyasar.vercel.app/).

## Getting Started

- (Optional) First and foremost, download [`Node.js`](https://nodejs.org/en/download) if you haven't before running this application,
- After opening a terminal window on a directory of your preference, run [`git clone https://gitlab.com/erhanyasar/next-todo-app.git`](https://gitlab.com/erhanyasar/next-todo-app.git),
- Run `cd next-todo-app`,
- Run `pnpm i` or `yarn` or `npm i`,
- Run `pnpm dev` or `yarn dev` or `npm run dev`,
- Finally open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Tech Stack

To be able to run a full-stack application without needing to have a separate database and a backend, [`Next.js`](https://nextjs.org/) and [`GraphQL`](https://graphql.org/) appeared
to be primary options initially. [`TypeScript`](https://www.typescriptlang.org/) was the next factor when deciding for type safety and many other benefits shipped with it.

Secondarily I used [`MUI (formerly Material UI)`](https://mui.com/), [`GraphQL Yoga`](https://the-guild.dev/graphql/yoga-server), and [`SWR`](https://swr.vercel.app/). Besides choosing [ESLint](https://eslint.org/) configuration ready shipped with Next.js, libraries like `lodash`, `notistack`, `uuid` were chosen to enhance application details.

Additionally, [`React Testing Library`](https://testing-library.com/docs/react-testing-library/intro/) and [`Jest`](https://jestjs.io/) are used together for testing purposes while [`Istanbul`](https://istanbul.js.org/) used as a code coverage tool.

Lastly, I make use of [GitLab](https://gitlab.com/) as a git provider and [Vercel](https://vercel.com/) as a hosting environment. Usually, I set up pipelines on GitLab if need be but, with Vercel, it's not needed to set up a separate CI/CD at all except for custom needs. With Vercel, it's possible to showcase the application on production for free.

### Architectural Choices

Next.js ships with the app router on default.

I prefer [Styled JSX](https://github.com/vercel/styled-jsx) even creating Next.js starter application bootstraps with a handful of styles with `global.css` and `page.module.css`. Apart from migrating them under `src/app/css`, the necessary part for the layout is kept and the rest of the template code within these files is removed.

Then the rest of the CSS development is kept via Styled JSX in order to keep everything at the same level to alter the developer experience.

Type declarations are collected under `src/types.ts` file and utility functions are collected under `src/utils` folder.

`MUI` is a pretty performant and enhanced UI library. So, apart from preferred components of it like `Card` for each to-do item, `notistack` is used as a complimentary library as an extra feature for notifications. Many easy-to-use component properties like `m={1}` to use within MUI components used instead of creating separate styles for each need.

`GraphQL Yoga` and `SWR` which work perfectly in combination used to sync data via implementation under `src/api/graphql/route.ts`.

The working mechanism of a Next.js application lays under the idea that the whole application renders within the `src/app/layout.tsx` and `src/app/page.tsx` respectively. Within `src/app/page.tsx`, `todoWrapper` is placed as a wrapper component to keep the rest of the necessary parts of the application. To keep the rest of the architecture simple, every single component and sub-component of the application is kept under `src/app/components`. There are three sub-components under `todoWrapper`;

- inputBar
- todosList
- buttonGroup

which each of these sub-components kept with `.tsx` extensions to their names mentioned above under `src/app/components` on the same hierarchy level with `todoWrapper.tsx`.

For branching over version control, [`Git Flow`](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) preferred. So apart from the main branch, there are 3 branches named `feature/todo-items` and `feature/todo-lists`, and `feature/todo-lists-with-improved-tests`. `feature/todo-lists` branch pull request (PR) is merged to the main branch as an update later on since it showcases enhanced capabilities. `feature/todo-lists-with-improved-tests` on the other hand, keeps extra tests pushed later on then the defined period for the assignment and intended to represent what's possible with an extra time without not modifying the previous work commited.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

### Additional Information

I wanted to share a little bit of extra information kind of off-topic from tech-level specifications. I understood the requirements as it should include only a to-do list and to-do list items within this list only when I read first. Then I realized the last sentence which is,

> Your solution should at least be able to create and delete todo lists, and create and add todo list items to a todo list.

Then I started to implement the necessary changes in order to provide solutions to both cases within different branches while keeping the latter one in the main branch.

So, apart from realizing last-minute work and then having to do necessary updates afterward, I continued to work a little bit more at the weekend, since I allocated less time than usual beforehand and the end of the defined period was the beginning of the weekend (It's a real-world scenario exactly to partly continue on the weekends for the works that I deliver sometimes for fun).

Even though I finished the fully functional application as an updated version before the defined period, I made updates how it looks and documentation afterward as it can be followed from the commit history.

Anyway, it made me spend less time on test coverage and implementation since I was going to spend the whole last day on testing and planned the time that way accordingly. So, I especially wanted to include tests that not failing and quickly fixed them instead within a branch that's named `feature/todo-lists-with-improved-tests`.

### Known Issues

1. There was an error due to testing implementation but it's gone now with latest improvements on the branch `feature/todo-lists-with-improved-tests` and it's PR is merged now.

2. Latest versions of applications bootstrapped with Next.js via `create-next-app`, ship with default warning of `Parsing error: Cannot find module 'next/babel'` for `next.config.js` with TypeScript.

```
{
  "extends": "next/core-web-vitals"
}
```

To workaround this error, the above configuration for `.eslintrc.json` is substituted with the one below. With this change, there's another

```
{
  "extends": ["next/babel", "next/core-web-vitals"]
}
```

With this change, another error was thrown in the build process even though everything working fine. It comes with Next.js version ^13 (I suppose) since I used previously the same fix without encountering it. Here below is what it looks like;

```
- info Linting and checking the validity of types ..- error ESLint: Failed to load config "next/babel" to extend from. Referenced from: /Users/erhanyasar/Documents/Computational/reference/repos/next-todo-app/.eslintrc.json
- info Linting and checking the validity of types
- info Collecting page data
- info Generating static pages (5/5)
- info Finalizing page optimization
```

## What's Next?

1. To be quick and showcase what's possible further as an intention, the `Persist Todos` button was added without implementation logic. Instead, there's a notification in case it's clicked.

2. Material UI's specifications for responsivity and many others like paddings, margin, grid used respcetively including edge cases like adding exceed amount of lists. Anyway with the help of a little bit more time, a better design solution to overall project can be provided since the design was not only consideration here.

### Learn More About Next.js

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app). To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

### Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
